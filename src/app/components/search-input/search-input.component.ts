import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {

  @Input() placeholder = 'Search ...';
  @Input() term = '';
  @Output() onTermChange = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
  }

  keyup(value: string): void {
    this.onTermChange.emit(value);
  }
}
