import {Component, OnInit} from '@angular/core';
import {BeersService} from '../../services/beers.service';
import {Observable} from 'rxjs';
import {Beer} from '../../models/beer';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit {

  beers$!: Observable<Beer[]>;

  constructor(private beersService: BeersService) {
  }

  ngOnInit(): void {
   this.updateList();
  }

  updateList(term?: string): void {
    this.beers$ = this.beersService.search(term);
  }
}
