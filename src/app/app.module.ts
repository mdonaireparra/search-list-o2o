import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SearchListComponent} from './pages/search-list/search-list.component';
import {BeersService} from './services/beers.service';
import {HttpClientModule} from '@angular/common/http';
import { TitleComponent } from './components/title/title.component';
import { SearchInputComponent } from './components/search-input/search-input.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchListComponent,
    TitleComponent,
    SearchInputComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [BeersService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
