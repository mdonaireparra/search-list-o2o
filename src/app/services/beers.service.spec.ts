import {TestBed} from '@angular/core/testing';

import {BeersService} from './beers.service';
import {HttpClientModule} from '@angular/common/http';

describe('BeersService', () => {
  let service: BeersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(BeersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
