import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Beer} from '../models/beer';

@Injectable({
  providedIn: 'root'
})
export class BeersService {

  private readonly endpoint = `${environment.api}/beers`;

  constructor(private http: HttpClient) {
  }

  search(foodTerm?: string): Observable<Beer[]> {
    let searchEndpoint = this.endpoint;
    if (foodTerm) {
      searchEndpoint = `${this.endpoint}?food=${foodTerm.trim().replace(' ', '_').toLowerCase()}`;
    }
    return this.http.get<Beer[]>(searchEndpoint);
  }
}
