import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SearchListComponent} from './pages/search-list/search-list.component';

const routes: Routes = [{
  path: '',
  component: SearchListComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
